<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Purifier;



class CommentsController extends Controller
{
  
    public function index(){

    	$comments = Comment::orderBy('created_at', 'DESC')->get();

    	return $comments;

    }

    public function store(){


    	$validated = request()->validate([
    		'pseudo' => 'required|min:3|max:20',
    		'email' => 'required|email',
            'title' => 'required|min:3|max:20',
            'content' => 'required|min:3|max:255',
            'rate' => 'required|numeric|between:1,5'
        ]);

       return Comment::create([
            'pseudo' => request('pseudo'),
            'email' => request('email'),
            'title' => request('title'),
            'content' => Purifier::clean(request('content')),
            'rate' => request('rate')
         ]);
  

    }

    public function edit($id){

        $comment = Comment::find($id);

        return $comment;

    }

    public function update($id){

        $comment = Comment::find($id);

        $validated = request()->validate([
            'pseudo' => 'required|min:3|max:20',
            'email' => 'required|email',
            'title' => 'required|min:3|max:20',
            'content' => 'required|min:3|max:250',
            'rate' => 'required|numeric|between:1,5'

        ]);

        $comment->update([
            'pseudo' => request('pseudo'),
            'email' => request('email'),
            'title' => request('title'),
            'content' => Purifier::clean(request('content')),
            'rate' => request('rate')
         ]);

        return $comment;


    }

    public function destroy($id){

        $comment = Comment::find($id);

        if($comment->delete()){
            return true;
        }

        
    }

    
}
