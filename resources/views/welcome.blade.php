<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&family=Raleway:wght@400;500;600;700&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <title>SuperShop</title>  
    </head>
    <body class="body">
        <header class="header">
            <h1 class="header__title heading-1">SUPER<span class="heading-1--blue">SHOP</span></h1>
        </header>
        <section class="product">
            <h3 class="product__title paragraph">Merchandising/Vetement/Haut/T-shirt</h3>
            <div class="product__show_article">
                <img class="product__image" src="img/shop.jpeg" alt="picture t-shirt">
            </div>
            <div class="product__description_article">
                <h3 class="product__name heading-2">T-SHIRT - NOIR </h3>
                <h5 class="product__price heading-3">15€</h5>
                <h5 class="product__average paragraph">T-shirt 100% coton made in France</h5>
                <p class="product__text paragraph" >Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui necessitatibus consequuntur est ad nemo? Quod quas aperiam quia. Explicabo, ipsam.</p>
                
            </div>
        </section>
        <div id="app" class="app">
            <comments></comments>
        </div>
        <footer class="footer">
            <p class="paragraph--blue">@copyright</p>
        </footer> 
         <script src="{{ asset('js/app.js') }}"></script>  
    </body>
</html>
